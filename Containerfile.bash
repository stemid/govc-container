#!/usr/bin/env bash

set -xeo pipefail

ctr=$(buildah from docker.io/alpine:latest)
buildah run "$ctr" sh -c 'apk update && apk upgrade'
buildah run "$ctr" apk add --no-cache bash curl ca-certificates gnupg coreutils

if [ ! -d tmp ]; then
  mkdir tmp
  curl -sL 'https://github.com/vmware/govmomi/releases/latest/download/govc_Linux_x86_64.tar.gz' | tar -xvzf - -C tmp
fi

buildah copy "$ctr" tmp/govc /usr/bin/govc
buildah run "$ctr" chmod 0755 /usr/bin/govc
buildah run "$ctr" adduser -D -h /home/alpine alpine

buildah config --entrypoint /bin/bash --author 'Stefan Midjich' \
  --workingdir '/home/alpine' --user alpine "$ctr"

buildah commit "$ctr" "localhost/${CI_PROJECT_NAME:-govc}:latest"
